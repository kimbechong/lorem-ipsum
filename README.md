# Lorem Ipsum Generator

## About the Project
Lorem ipsum is dummy text used in laying out print, graphic or web designs. This Spring Boot project generates dummy text py passing in the number of words you want.

## Getting Started
You can access the project through Docker or locally.

### Docker
Pull in and run the Docker image from Docker Hub with the CLI
```
docker run -d -p 80:8080 kimbechong/lorem-ipsum
```

### Running Locally
1. Build the project
```
./mvnw clean install
```

2. Run the project
```
./mvnw spring-boot:run
```

## Usage
1. Open your browser
2. Enter `http://localhost:80/lorem-ipsum/<num>`
3. Replace `<num>` in the request with any number to indicate the amount of dummy words to retrieve
   * Example: http://localhost:80/lorem-ipsum/10
