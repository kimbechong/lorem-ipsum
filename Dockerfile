FROM openjdk:11-jdk-slim

WORKDIR /

COPY target/lorem-ipsum-0.0.1-SNAPSHOT.jar loremipsum.jar

EXPOSE 8080

CMD java -jar loremipsum.jar