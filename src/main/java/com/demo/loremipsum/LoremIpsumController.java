package com.demo.loremipsum;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoremIpsumController {

    @GetMapping("/lorem-ipsum/{num}")
    public String getLoremIpsum(@PathVariable int num) {
        Lorem lorem = LoremIpsum.getInstance();
        return lorem.getWords(num);
    }

}
